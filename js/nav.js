document.addEventListener("click", clickListener);

function clickListener(event) {
    if (event instanceof MouseEvent){
        var width = document.body.clientWidth;
        var height = document.body.clientHeight;

        if (event.clientX > 200 && nav == 1){
            moveNav();
        }
        if ((event.clientY < (height - 288) || (event.clientX < (width * 3 / 4) && width > 800)) && form == 1){
            moveForm();
        }
    }
}
var nav = 0;
function moveNav(from) {
    if (nav == 1) {
        document.getElementById('navprinc').style.left = "-200px";
    } else {
        document.getElementById('navprinc').style.left = "0px";
    }
    nav = (nav + 1)%2;
}
