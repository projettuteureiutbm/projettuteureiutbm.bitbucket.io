var form = 0;
function moveForm(from){
    var elements = document.getElementById('formulaire');
    var style = elements.style;
    var description = document.getElementsByClassName('description');

    if (from == "mail" && form == 1){
        return;
    }
    if (form == 0){
        description[0].style.display = "block";
        style.transition = "box-shadow 0.3s, top 0.5s 0.3s, height 0.5s 0.3s"
        style.top = "-238px";
        style.height = "288px";
        style.boxShadow = "0px 0px 5px black";
    } else if (form == 1) {
        description[0].style.display = "none";
        style.transition = "box-shadow 0.3s 0.5s, top 0.5s, height 0.5s"
        style.top = "0px";
        style.height = "50px";
        style.boxShadow = "0px 0px 0px black";
    }
    form = (form + 1) % 2;
}
