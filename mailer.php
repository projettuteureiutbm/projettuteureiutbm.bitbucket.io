<?php

if (!isset($secure) || $secure != true){
    header('Location: index.php');
}
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

if (isset($_POST["email"]) and isset($_POST["prenom"]) and isset($_POST["nom"])) { //supprimer false pour que le mailer fonctionne
    $email = $_POST["email"];
    $nom = $_POST["nom"];
    $prenom = $_POST["prenom"];

    require 'PHPMailer-master/src/Exception.php';
    require 'PHPMailer-master/src/PHPMailer.php';
    require 'PHPMailer-master/src/SMTP.php';

    $mail = new PHPMailer(true);

    try {
        $mail->isSMTP();
        $mail->IsHTML(true);
        // $mail->SMTPDebug = 2;
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = 587;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Username = 'projettuteuredrones@gmail.com';
        $mail->Password = 'ptdrones8';
        $mail->setFrom('Drones@no-reply.com', 'Drones');
        $mail->addAddress($email, $nom.' '.$prenom);
        $mail->Subject = 'Drones : Informations complémentaires';
        $mail->Body = "
            Bonjour $prenom $nom<br>
            Vous allez recevoir prochainement des informations complémentaires concernant les drones<br>
            <br>
            L'équipe de projet tuteuré<br>
        ";

        if (!$mail->send()) {
            echo'
                <script type="text/javascript">
                alert("test");
                displayNotif(1);
                </script>
            ';
        } else {
            echo'
                <script type="text/javascript">
                displayNotif(0);
                </script>
            ';
        }
    } catch (Exception $e) {
        echo'
            <script type="text/javascript">
            displayNotif(1);
            </script>
        ';
    }
}
?>
