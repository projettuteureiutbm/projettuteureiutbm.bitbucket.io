<?php
    $secure = true;
    $Title = 'Drones de secours';
    include 'header.php';
?>

<?php include 'nav.php'; ?>

<div class="container">
    <!-- Concenu sur les drones de secours -->
    <h2>Les Drones de secours</h2>
    <div class="row" >
        <div class="col-6">
            <p>
                Plusieurs applicaton existent pour les drones de secours. Ce sont des auxiliaires au services de policiers,
                pompiers ou équipes de secours, ils aident a la mission de recherche et/ou de sauvetage.
            </p>
        </div>
        <div class="col-6">
            <iframe width="100%" height="200px" src="https://www.youtube.com/embed/n4ZtzaOfVrQ" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>

    <section>
        <h3>Droit</h3>
        <div class="row">
            <div class="col-6">
                <p>
                    Les drones secouristes sont une innovation encore en développement.<br>
                    Cependant, pour ces drones actifs, des réglementations s'imposent. Ce sont les
                    mêmes que pour les drones à usage professionnels. Cette réglementation fait partie
                    au préalable de plusieurs déclaration d'activité et de notification au ministère des
                    armées avant utilisation.
                </p>
                <p>
                    Cependant, les entreprises développant les drones secouristes de demain
                    doivent tout de même passer par la case réglementation. Pour une activité,
                    il est exigé de nombreuses informations auprès de la DSAC (direction
                    de la sécurité de l’Aviation civile) suivie de restrictions, cependant plus permises
                    comparé à une utilisation non professionnelle.
                </p>

            </div>
            <div class="col-6">
                <p>
                    Un espace drone est disponible sur le site du ministère de la transition
                    écologique et solidaire afin de faciliter les démarches pour une utilisation
                    professionnelle d'un drone cet espace permet entre autres de :
                </p>
                <ul>
                    <li>Gérer leurs données personnelles</li>
                    <li>Déclarer l'activité</li>
                    <li>Réaliser des bilans d'activité</li>
                    <li>Notifier les vols exceptionnels auprès du ministère des armées</li>
                    <li>Consulter l'historique des démarches</li>
                </ul>
            </div>
        </div>
    </section>
    <section>
        <div class="row">
            <p>
                Il existe différentes applications dans les innovations des drones
                secouristes : Ces drones peuvent servir pour de multiples services en voici quelques
                exemples :
            </p>
        </div>
    </section>
    <section>
        <h3>Le drone des plages</h3>
        <div class="row">
            <div class="col-6 resp-12">
                <p>
                    Un drone nommé « Helper » est en service depuis le 14 juillet sur les plages
                    d’Aquitaine, ce drone à pour but de faire gagner du temps au maîtres-nageurs en
                    épaulant la personne consciente en état de noyade. Il va ainsi, en quelques
                    secondes, parvenir à la cible et déployer une bouée connectée
                </p>
            </div>
            <div class="col-6 resp-12">
                <img src="media/helper" alt="Drone Helper" width="100%">
            </div>
        </div>
    </section>
    <section>
        <h3>Drones For Good</h3>
        <div class="row">
            <div class="col-6 resp-12">
                <img src="media/dronesforgood" alt="Drones For Good" width="100%">
            </div>
            <div class="col-6 resp-12">
                <p>
                    Alec Momont est le créateur d’un projet nommé « Drones For Good ». Ce
                    projet à pour but de mettre à disposition des ambulanciers, des drones de soins
                    d’urgence. Son premier drone, dont il a fait la promotion via un trailer, permet à toute
                    personnes de réanimer tout patient atteint d’une attaque cardiaque. Après appel d’un
                    numéro d’urgence, un drone se met en route et peut, en une minute, atteindre sa
                    cible (tandis qu’une ambulance peut prendre 10 min). Arrivé à destination et après
                    fixation des électrodes sur le patient, celui-ci est réanimé à distance
                </p>
            </div>
            <div class="col-6">
                <p>
                    D’après Alec Momont, les exemples d’utilisation des drones secouristes sont
                    multiples, par exemple pour secourir les victimes d’avalanches : Après une
                    avalanche, cela peut prendre plus d’une heure pour les secours d’arriver sur les
                    lieux. Un drone est alors fortement utile pour permettre de localiser, par des caméras
                    15/34thermiques, les personnes ensevelies, et ainsi diminuer fortement le temps de
                    recherche des corps lors de l’arrivée des secours.
                </p>
                <p>
                    Un autre exemple d’utilisation, selon Alec Momont, serait de porter des
                    produits légers en urgence tels que des anti venins ou des injections anti allergique.
                </p>
            </div>
            <div class="col-6">
                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/y-rEI4bezWc" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
            </div>
        </div>
    </section>
    <section>
        <div class="row">
            <div class="col-6">
                <p>
                    Pour reprendre l’exemple d’utilisation en montagne, Land Rover a sorti une
                    division de voiture SVO (Special Vehicle Operation) Baptisé « Project Hero ». Ce 4x4
                    est couplé avec un drone dans son toit repliable. Ainsi, lors d’un accident en
                    montagne, ce drone permet de repérer l’environnement et d’accéder plus rapidement
                    et en sécurité sur les lieux d’un accident.
                </p>
            </div>
            <div class="col-6">
                <img src="media/projecthero.jpg" alt="Project Hero" width="100%">
            </div>
        </div>
    </section>
    <section>
        <h3>Au service des pompiers</h3>
        <div class="row">
            <div class="col-12">
                <p>
                    Certaines équipes de pompiers se servent des drones pour leurs opérations
                    de sauvetage, cela évite un repérage des lieux qui peut durer parfois jusqu’à
                    plusieurs heures et permet, lors des incendies, de facilement détecter les points
                    chauds grâce à une caméra infrarouge fixé sur le drone. Certains pompiers utilisent
                    aussi le drone comme un moyen de déclencher des incendies préventifs sous
                    contrôle
                </p>
            </div>
        </div>
    </section>
    <section>
        <h3>Le Rwanda</h3>
        <div class="row">
            <div class="col-6">
                <img src="media/rwanda" alt="Au Rwanda" width="100%">
            </div>
            <div class="col-6">
                <p>
                    Pour finir, le Rwanda (pays de l’Afrique de l’Est) a investi en 2106 dans un
                    centre de stockage médical livrant le sang aux hôpitaux dans un rayon de 75km
                    autour de la base. Le Rwanda est l’un des pays les plus pauvre du globe, les
                    infrastructures sont peu développées, il est donc difficile de circuler rapidement dans
                    le pays. Pour répondre à cette demande de livraison urgente, le Rwanda a fait appel
                    à une startup californienne (Zipline) afin de mettre au point un drone pouvant livrer
                    des pochettes de sang en moins de 30 minutes à travers les collines du pays. La
                    ministre de la santé rwandaise a tenu à montrer l’exemple en disant que si l’un des
                    pays les plus pauvres du monde à pu mettre au point une telle innovation, alors les
                    autres pays peuvent aussi le faire
                </p>
            </div>
        </div>
    </section>
    <section>
        <h3>Conclusion</h3>
        <div class="row">
            <p>
                Les drones secouristes sont en plein expansion et, pour certains, ont
                récemment commencé leurs activités. Cependant, même s’il existe différents types
                de drones de ce type, ils ont tous un point en commun : faire gagner du temps
            </p>
        </div>
    </section>
</div>

<?php
    include 'footer.php';
?>
