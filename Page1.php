<?php
    $secure = true;
    $Title = 'Drones civils/de loisir';
    include 'header.php';
?>

<?php include 'nav.php'; ?>

<div class="container">
    <h2>Drones civils</h2>
    <div class="row col-12">
      <p>
          Il s’agit des drones destinés à l’usage privé, en particulier pour le loisir.
          Cette catégorie de drones a récemment connu une montée de popularité :
          Les utilisations sont multiples filmer les paysages, les activités sportives,
          certains sont adaptés aux enfants, et d’autres sont des jouets. DJI est le principal vendeur de drones “civils”,
          en possédant 75% du marché. La multiplication de ces drones conduit à une méfiance générale, voire un scepticisme :
          les accidents inquiètent, et des réglementations ont été mises en place pour limiter les survols dangereux.
      </p>
    </div>
    <section>
        <h3>Droit</h3>
        <div class="row">
            <div class="col-6 resp-12">
                <p>
                    Depuis quelques années, les drones de loisir sont vendus en masse, adaptés aux enfants ou aux amateurs.
                    La multiplication des vols de drones peut conduire à des accidents,
                    et c’est pourquoi les gouvernements ont imposé des réglementations pour limiter les risques et encadrer la pratique.
                    Voici les 10 réglementations en vigueur en France, en 2017,
                    stipulées par le ministère de l’écologie et du développement durable :
                </p>
            </div>
            <div class="col-6 resp-12">
                <ul>
                   <li>Il est interdit de survoler quelqu’un (danger de chute sur l’individu)</li>
                   <li>Il est interdit de faire voler son drone à plus de 150m du sol (Si le drone est loin,
                       il risque d’échapper au contrôle du pilote, et un accident peut arriver)</li>
                   <li>Ne jamais faire voler son drone de nuit, et toujours le garder en vue
                       (encore une fois pour des raisons évidentes de sécurité)</li>
                   <li>Il est interdit de faire voler un drone en pleine ville, dans une zone publique</li>
                   <li>Il est aussi interdit de faire voler son drone dans des zones d’aviation,
                       comme les aéroports et les aérodromes, à part si un espace y est dédié.</li>
                   <li>Il est interdit de survoler les zones sensibles, comme les terrains militaires, les voies ferrées ou les hôpitaux.</li>
                   <li>En particulier pour les drones possédant une caméra, il faut respecter la vie privée des gens.</li>
                   <li>Tout contenu filmé ou pris en photo par un drone doit posséder l’accord des personnes apparaissant dessus avant d’être publié.</li>
               </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-6 resp-12">
               <p>
                   Ces règles n’étant que très peu respectées et les infractions se multipliant,
                   une nouvelle réglementation est prévue pour juillet 2018, censée renforcer le contrôle sur le engins volants :
               </p>
            </div>
            <div class="col-6 resp-12">
               <ul>
                   <li>Tout drone dont le poids dépasse 800g doit être déclaré,
                       et comporter des signaux lumineux et sonores afin d’être repéré facilement dans le ciel.</li>
               </ul>
            </div>
        </div>
        <div class="row col-12">
            <p>
                Les différents gouvernements tirant la sonnette d’alarme à propos des infractions de plus en plus fréquentes,
                les constructeurs de drones ont pris des mesures visant à les limiter : ainsi, certains drones de loisirs
                (le DJI spark par exemple) ne peuvent plus décoller dans certaines zones prédéfinies,
                notamment les zones militaires. Mais certains gouvernements, inquiétés par les délits, prennent eux-mêmes des mesures.
                Par exemple, American Airspace system a créé un drone équipé d’un filet, ”l’interceptor”,
                qui est capable d’intercepter un drone illégal en plein vol, autour des zones d’aviation.
                La gendarmerie française, elle, a choisi d’entraîner des rapaces pour arrêter les engins dangereux,
                moins technologiques mais autant, voire plus fiables.
            </p>
        </div>
    </section>
    <section>

    </section>
    <section>
    <h3>économie</h3>
        <div class="row col-12">
            <p>
                Les drones civils, ou de loisir, sont destiné aux particuliers.
                Leur succès commercial est très récent, commençant environ en 2010,
                et les constructeurs ont diversifié les types d’engins, pour toucher un plus grand public.
            </p>
        </div>
        <div class="row">
            <div class="col-6 resp-12">
                <p>
                    Il existe des drones ‘jouets’, pour les enfants (Parrot est le principal constructeur):
                    Il s’agit de drones simples, très peu performants, destinés à une utilisation de courte portée,
                    pour sensibiliser les enfants à l’utilisation des drones.
                    On notera qu’il s’agit d’une technique visant à vendre des drones de meilleure qualité dans le futur.
                </p>
            </div>
            <div class="col-6 resp-12"><img src="media/mini.png" alt="drone petit" width=75% ></div>
        </div>
        <div class="row">
            <div class="col-6">
                <p>
                    Il existe aussi et surtout des drones “bon marché”, qui ont une petite autonomie (en moyenne une dizaine de minutes),
                    filment ou prennent des photos, mais restent peu chers. C’est cette catégorie de drones qui connaît le plus grand succès,
                    depuis peu.
                </p>
            </div>
            <div class="col-6"><img src="media/moyen.png" alt="drone moyen" width=100% ></div>
        </div>
        <div class="row">
            <div class="col-6">
                <p>
                    Finalement, il y a des drones de haute qualité,
                    le plus souvent utilisés pour filmer les paysages ou les sports, avec des caméras performantes,
                    une meilleure stabilité, et une grande autonomie. Bien sûr, les prix en sont augmentés, allant jusqu’à 2000 euros.
                </p>
            </div>
            <div class="col-6"><img src="media/gros.png" alt="beau drone" width=100% ></div>
        </div>
        <div class="row col-12">
            <p>
                Le marché est bon, car encore peu de constructeurs ont su percer. Les principaux vendeurs,
                par ordre de part de marché (en 2016), sont :
            </p>
            <ul>
                <li>DJI</li>
                <li>Parrot</li>
                <li>Xiaomi</li>
                <li>Hover</li>
                <li>AeroVironment</li>
            </ul>
            <p>
                Ces entreprises surfent sur la vague de cette technologie depuis quelques années déjà :
                Parrot, le constructeur français numéro 1, a vu ses ventes grimper de 350% en un trimestre,
                début 2015. Cet essor a motivé de nombreux auto-entrepreneurs, qui étaient environ 750 en 2015,
                et dont le nombre ne cesse d’augmenter. Mais, début 2017, le secteur des drones civils a connu un important “trou d’air”,
                et beaucoup d’entreprises, dont Parrot, ont du se séparer d’une partie de leurs effectifs,
                certaines même ont jeté l’éponge. Cela s’explique par un désintéressement des particuliers,
                même si les statistiques montrent que le marché reste prometteur.<br>
                DJI, le leader mondial du drone de loisir, a réussi à éviter cette crise, et détient à présent la majeure partie du marché.
                Le drone civil ne va pas disparaître, mais c’est plutôt le drone professionnel,
                comme par exemple dans le cinéma ou l’agriculture, qui va se développer.<br>
                Les nouvelles entreprises ne devraient donc pas se spécialiser dans le drone de loisir,
                mais faire comme amazon qui, par exemple, projette de créer des drones capables de livrer les commandes bien plus efficacement que la poste.
                Ce genre de projet peut, d’ailleurs, mener à beaucoup de débats : cela signifie forcément que des postes seront supprimés,
                les drones devenant de plus en plus autonomes. Bientôt, les drones totalement autonomes apparaîtront,
                et il suffira de le programmer pour qu’il remplisse sa tâche sans aucune assistance.
            </p>

        </div>
        <div class="row col-12">
            <p>
                Le marché du drone civil connaît donc quelques difficultés, mais tient bon.
                Les constructeurs se diversifient pour répondre à la demande d’une plus large clientèle,
                et le futur du drone en général est prometteur. Il s’agit d’une nouvelle technologie,
                dans le sens où, comme la réalité virtuelle, son utilisation s’est très largement développée depuis peu,
                et se démocratise. Le futur dira si le drone perdurera ou deviendra une technologie en marge, remplacée par une autre...
            </p>
        </div>
    </section>
</div>
</div>

<?php
    include 'footer.php';
?>
