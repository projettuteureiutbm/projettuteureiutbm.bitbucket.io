<?php
if (!isset($secure) || $secure != true){
    header('Location: index.php');
}
?>
<footer id="footerprinc">
    <div class="row">
        <div class="col-9 resp-12">
            <p>
                Site créé par Corentin, Quentin, Kylie et Nathan
            </p>
        </div>
        <script type="text/javascript" src="js/footer.js">
        </script>

        <div class="col-3 resp-12 formulaire" id="formulaire">
            <p class="description">Vous voulez plus d'informations? donnez-nous votre email et nous vous tiendrons au courant !</p>
            <form action="index.php" method="post">
                <div>
                    <label for="email">Votre email : </label>
                    <input type="email" id="email" name="email" onfocus="moveForm('mail')">
                </div>
                <div>
                    <label for="prenom">Votre prenom : </label>
                    <input type="text" id="prenom" name="prenom" onfocus="moveForm('mail')">
                </div>
                <div>
                    <label for="nom">Votre nom : </label>
                    <input type="text" id="nom" name="nom" onfocus="moveForm('mail')">
                </div>
                <input class="valid" type="submit" value="ok !" name="ok">
            </form>
        </div>
    </div>

    <script type="text/javascript" src="js/notif.js">
    </script>
    <?php include 'mailer.php'; ?>
</footer>
</body>
</html>
