<?php
    $secure = true;
    $Title = 'Drones de compétition';
    include 'header.php';
?>

<?php include 'nav.php'; ?>

<section class="container">
    <h2>Les drones de compétition</h2>
        <h3>Histoire</h3>
        <section>
            <div class="row">
                <div class="col-6">
                      <p>
                        Comme pour toute chose, les personnes aiment se différencier les unes des autres en cherchant à savoir qui est le meilleur, c’est ainsi que les premières compétitions se sont crées. Au début il y a eu beaucoup de problèmes, les compétitions étaient non réglementées non assermentées augmentant donc parallèlement les risques d’accident suite à une sortie de route ou un mauvais passage de porte.
                        En France il a fallu attendre la fin de l’année 2015 pour voir se démocratiser le phénomène, tandis que en Australie ce type de compétition a connu un grand engouement fin 2014.
                        Les drones qui peuvent rentrer en lisses dans ce type de compétition valent d’une centaines à plusieurs milliers d’euros, en plus des drones il y a bien souvent une équipe de technicien qui paramètre le drone au maximum pour chaque course afin d’en tirer le meilleur parti possible, ce type de compétition est donc très semblable aux compétitions de courses automobiles.
                        <br>Un exemple de drone pour les compétition FPV :<br>
                      </p>
                </div>
                <div class="col-6">
                        <img src="media/dronefpv.jpg" alt="drone FPV" width=100% >
                </div>
            </div>
        </section>
    <br>
    <br>
        <h3>Juridiction</h3>
        <section>
            <div class="row">
                <div class="col-6">
                  <p>
                    Au commencement les compétitions de drones que ça soit de vitesse ou d’agilité se faisaient avec des drones non dotées de caméra, cela engendrait des problèmes dans l'appréciation des distances de la part du pilote, ce type de compétition a donc été rapidement interdite. Puis il y a eu l’arrivée des drones à caméra embarquée, nous allons donc nous intéresser tout particulièrement à la juridiction de ces derniers puisqu’ils en ont permis le développement.
                    Les compétitions de drones à caméra embarquée avec utilisation de lunette de réalité virtuelle (on simplifiera ici par l’abréviation FPV) ont des règles pourtant sur les drones mais aussi sur les spécificités du circuits, afin de réduire les risques de blessures au maximum.
                    Tout d’abord intéressons nous aux règles sur les drones. La masse maximale du drone avec la batterie doit être de 1kg au maximum et la puissance de ses moteurs ne doit pas dépasser 17 volts, celle ci est mesurée batterie chargée avant le vol. Le modèle doit être suffisamment petit également afin d’éviter un mouvement cinétique trop important puisque la distance entre les axes des moteurs doit être inférieure à 330m. Bien évidement les hélices entièrement métal sont interdites. Assez curieusement tout système de protection des hélices est interdit. Cette règle est faite de manière a ce que le drone tombe au sol directement après un impact sur une porte pour éviter qu’il rebondisse dans les spectateurs. Ils doivent impérativement posséder un système “fail-safe” pour couper toute motorisation lors du déclanchement.

                    Comme nous l’avons dit précédemment il y a aussi des règles sur les pistes où vont se dérouler les compétitions FPV. Il doit avoir une longueur développée minimale de 250m en champ libre et dans 80m en salle ou dans un bois (on parle donc de circuit court). L’organisateur doit s’assurer de la bonne réception du signal qu’il soit de la télécommande ou du flux vidéo peut importe la position de l’engin sur le circuit. Une ligne de sécurité délimitant l’aire de vol doit être obligatoirement marquée au sol. Chaque virage doit être marqué par un drapeau visible. Le circuit doit être tracé dans le but de prévenir les sorties accidentelles de la zone de course. Toute trajectoire de retour vers la ligne de sécurité se fera en direction d’un espace sécurisé sans concurrents, ni public.
                  </p>
                </div>
              <div class="col-6">
                <img src="media/parcoursfpv.jpg" alt="drone FPV" width=100% >
              </div>
            </div>
        </section>
    <br>
    <br>
          <h3>Actualité</h3>
          <section>
              <div class="row col-12">
                <p>
                  En 2015, le prince héritier de Dubaï avait lancé une compétition géante de FPV dénommé “World Drone Prix” avec un million de dollars de dotation pour le gagnant, il y a eu des qualifications aux quatres coins du globe. En fin 2016, il y a également eu un grand concours à Hawaï avec une cagnotte de $200.000. En France cette année à étée pas mal agitée en compétition grâce à la baisse des prix et à la démocratisation de ce type de compétition, il y a par exemple eu une compétition de rang international à Lyon la “Lyon FPV Wold Wup”.
                  Les compétitions de drones plaisant de plus en plus aux personnes, France 4 a décidé de commencer le tournage d’une compétition indoors de drones. Les tournages se feront en public, et un grand nom de la télévision s’est rajouté au casting Alex Goude (Incroyable talent, Défis Cobayes, …). Diffusion probable début 2018.
                  <br>Nous pouvons donc voir que l’avenir des compétitions FPV est sous de bonnes augures.
                </p>
              </div>
          </section>
</section>

<?php
    include 'footer.php';
?>
