<?php
if (!isset($secure) || $secure != true){
    header('Location: index.php');
}
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap-reboot.css">
    <link rel="stylesheet" href="css/header.css">
    <link rel="stylesheet" href="css/nav.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/basic.css">

    <title><?php echo $Title; ?></title>

</head>
<body>
    <header id="header">
        <div class="button" onclick="moveNav('open')" onmouseover="test()">
            <img id="icon" src="media/menu.png" alt="icon nav">
        </div>
        <h1>Les Drones</h1>
    </header>
    <!-- <div class="behind"></div> -->
    <div class="notification ok">
        un email a été envoyé
    </div>
    <div class="notification notok">
        echec lors de l'envoi du mail
    </div>
