<?php
    $secure = true;
    $Title = 'Drones militaires';
    include 'header.php';
?>

<?php include 'nav.php'; ?>

<section class="container">
    <h2>Les drones militaires</h2>
    <h3>Historique</h3>
    <section>
        <div class="row">
            <div class="col-6 resp-12">
                <p>
                    L'armée de l'air française commence à utiliser des drones comme cibles volantes à partir de la fin des années 1950, le premier vol d'essai avec un Vampire 1 ayant lieu le 12 octobre 1957.
                    Les premiers drones de renseignement sont mis en service en 1995 et engagés au Kosovo en 2001.
                    Ils sont d’abord utilisés pour la reconnaissance puis apparaissent des versions armées. Cela permet d’effectuer des opérations plus discrètes et limitent les pertes humaines.
                    <br>Voici un drone utilisé par l'armée française :<br>
                </p>
            </div>
            <div class="col-6 resp-12">
                <img src="media/drone.jpg" alt="drone français" width=100% >
            </div>
        </div>
    </section>
    <h3>Economie et juridiction</h3>
<section>
    <div class="row">
        <div class="col-6 resp-12">
            <p>
                Bien entendu, l’achat de drones militaires est impossible par un civil et ses armes sont développées dans le plus grand secret. Un pays peut décider de vendre ses drones à un autre comme en acheter. Les chiffres concernant les bénéfices tirés des ventes de drones inter-étatiques ou entre sociétés privées sont difficiles à trouver car cela reste dans le domaine militaire donc classé secret-défense.
                On voit ici que le marché des drones militaires représente plus de la moitié du marché international :<br>
            </p>
        </div>
        <div class="col-6 resp-12">
          <br><img src="media/graph.jpg" alt="graphique" width=100%><br>
        </div>
        <p>
            Les drones militaires ne sont pas soumis à des restrictions particulières comme les drones du civil. Ils sont placés sous la responsabilité de l’armée qui en fait l’utilisation qu’elle souhaite. L’évolution qualitative et quantitative des drones militaires a conduit à la création de lois permettant de sécuriser l’espace aérien ainsi que les zones survolées. Ainsi, la fabrication de tels types de drones possède des normes : un certificat de type (équivalent de la carte grise) est délivré pour chaque nouveau drone, celui-ci est essentiel pour l’utilisation du drone. Suite à ce certificat, le drone se voit délivrer un certificat de navigabilité (équivalent au contrôle technique automobile)  qui devra être respecté tout au long de la durée de vie du drone. Cela sous-entend de nombreuses règles de maintenance strictes.
            De plus, les drones militaires sont contraints de voler dans des espaces aériens inoccupés car il leur est impossible de suivre la règle principale de trafic aérien : “voir et éviter” qui permet d’éviter les collisions inter-aéronefs. En effet, la plupart des drones militaires ne disposent au mieux que d’une petite caméra frontale en complément du matériel militaire (radar, armement). Par nature, un drone ne possède pas de pilote pouvant veiller à cette règle. On peut donc parler de “détecter et éviter”.
        </p>
    </div>
</section>

<h3>Fonctionnement</h3>
<section>
    <div class="row">
        <p>
            On observe plusieurs utilisation de drones dans l’armée. Ils évoluent dans différents milieux : terrestre, aérien, aquatique. Plusieurs fonctions leur sont attribuées comme le transport de troupes ou matériel, le combat, la reconnaissance, le déminage, la détection de produits dangereux ou servir de leurre.
            Leur utilisation est appréciée car elle permet une meilleure rapidité d’exécution et de limiter les pertes humaines. Néanmoins, leur production, maintenance et  utilisation est relativement coûteuse.
            Récemment, la France a décidé d’armer ses drones de reconnaissance et de renseignement. L’armée américaine a également testé le premier drone a hélices (hélicoptère). Actuellement, l’armée américaine demeure la plus grande utilisatrice de drones.
        </p>
    </div>
</section>
<h3>Actualité</h3>
<section>
  <div class="row">
    <div class="col-6 resp-12">
        <p>
            Étonnement, en 2013, l’armée française a conçu le drone Reaper pour appuyer ses troupes au sol dans le Sahara sans obtenir de certificat de type mais a obtenu une dérogation exceptionnelle.
            Effectivement, il a été conçu pour des opérations extérieures et non pour le survol de zones civils. Son vol est donc de plus en plus fortement restreint en fonction de la population (les zones très peuplées ne doivent être survolées que brièvement. Néanmoins, en conditions d’opération, cette règle peut être dérogée contrairement au survol du territoire français lors d’entraînements ou de formations, durant lequel les restrictions s’appliquent.
            Actuellement, il n’est pas en service car problèmes techniques, mais lorsqu’il le sera,il devra voler dans des couloirs aériens sécurisés fermés en raison de l’impossibilité de respecter la règle “voir et éviter” vue précédemment.
            L’armée de terre possède également des drones. Par exemple, le drone de patrouille Patroller certifié en tant qu’ avion piloté. Afin de décrocher la certification de drone, l’armée de terre a adapté ses méthodes de maintenance pour s’adapter aux normes de navigabilité. Il devrait donc entrer en service au cours de l’année 2018.<br>
        </p>
    </div>
    <div class="col-6 resp-12">
        <img src="media/patroller.jpg" alt="patroller" width=100%>
    </div>
  </div>

</section>
</section>

<?php
    include 'footer.php';
?>
