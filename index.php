<?php
    $secure = true;
    $Title = 'Menu';
    include 'header.php';
?>

<?php include 'nav.php'; ?>

<section class="container">
    <!-- index -->
    <div class="row presentation">
        <p>
            Voici le site de présentation de notre projet tuteuré portant sur les drones.<br>
            Vous pourrez accéder aux différentes pages à l'aide des liens ci-dessous.
        </p>
    </div>
    <div class="row pres">
        <a href="Page1.php" class="col-3 presblock civils">Drones civils</a>
        <a href="Page2.php" class="col-3 presblock militaires">Drones militaires</a>
        <a href="Page3.php" class="col-3 presblock competitions">Drones de compétitions</a>
        <a href="Page4.php" class="col-3 presblock secours">Drones de secours</a>
    </div>
</section>

<?php
    include 'footer.php';
?>
